package com.example.savenqueteur.logic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.savenqueteur.R;
import com.example.savenqueteur.activity.MainActivity;
import com.example.savenqueteur.dto.Enquete;
import com.example.savenqueteur.dto.Position;
import com.example.savenqueteur.firebase.DataListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kevin on 11/04/18.
 */

public class MapHandling implements Serializable{

    private static FusedLocationProviderClient mFusedLocationClient;
    private static Activity activity;
    private static LocationRequest mLocationRequest;
    private static LocationCallback locationCallback;
    private static GoogleMap mMap;

    //all linked to Enquete
    private static Enquete enquete;
    private static double distance = 0;
    private static List<Position> positions = new ArrayList<>();

    //Last position
    private static Position lastPosition;

    private static float[] distances = new float[1];

    private MapHandling() {

    }

    public static void getInstance(Activity activity) {
        MapHandling.activity = activity;
        if (mFusedLocationClient == null) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        }
    }


    /**
     * move the camera to the user position
     */
    public static void moveCameraToUserPosition() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            LatLng currentLocation;
                            if (location != null) {
                                // Logic to handle location object
                                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

                                Log.e("###### ToUserPosition " , " " + location.getLatitude() + " " +location.getLongitude() + " ");

                                mMap.addMarker(new MarkerOptions().position(currentLocation).title(activity.getString(R.string.youHere)));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 13.0f));
                            }
                        }
                    });

        } catch (SecurityException e) {
        }
    }

    public static void moveCameraToPoi() {

        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, new OnSuccessListener<Location>() {


                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.


                            LatLng currentLocation;

                            if (location != null) {

                                // Logic to handle location object
                                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

                                Log.e(" Enquete ",MainActivity.getEnquete().toString());

                                Log.e(" Description poi ",MainActivity.getEnquete().getDescription());

                                int nbPois = MainActivity.getEnquete().getPois().size();
                                String nbpois = "" + nbPois ;

                                Log.e(" NB POIS:", nbpois);

                                for (int i = 0; i < MainActivity.getEnquete().getPois().size(); i++) {

                                    MapHandling.addMarker(MainActivity.getEnquete().getPois().get(i).getPosition().getLatitude(),
                                            MainActivity.getEnquete().getPois().get(i).getPosition().getLongitude(), MainActivity.getEnquete().getPois().get(i).getTitle());
                                    Log.e("##### POI nb : " + i + 1," #### " + MainActivity.getEnquete().getPois().get(i).getPosition().getLatitude());
                                }

                                //mMap.addMarker(new MarkerOptions().position(currentLocation).title(activity.getString(R.string.youHere)));
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 13.0f));
                            }

                            // Logic to handle location object
                           currentLocation = new LatLng(location.getLatitude(), location.getLongitude());

                          //  mMap.addMarker(new MarkerOptions().position(currentLocation).title(activity.getString(R.string.youHere)));
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 13.0f));

                        }
                    });
        } catch (SecurityException e) {
        }
    }


    /**
     * get the user latitude and longitude
     * @param dataListener
     */
    public static void getUserLatLng(final DataListener dataListener) {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {

                                Log.e("###################"," getUserLatLng");

                                // Logic to handle location object
                                dataListener.onSuccess(location);
                            }
                        }
                    });
        } catch (SecurityException e) {
        }
    }

    /**
     * set the user current position on the map
     */
    public static void setUserCurrentPosition() {
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            LatLng currentLocation;
                            if (location != null) {
                                // Logic to handle location object
                                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                                mMap.addMarker(new MarkerOptions().position(currentLocation).title("You are here"));
                                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
                                positions.add(new Position(location.getLongitude(), location.getLatitude(), location.getAltitude(), new Date().toString()));
                            } else
                                Toast.makeText(activity, activity.getString(R.string.youHere), Toast.LENGTH_LONG).show();
                        }
                    });
        } catch (SecurityException e) {
        }
    }


    //CREATE ENQUETE METHOD ?

    /**
     * add marker on the map
     * @param latitude
     * @param longitude
     * @param text
     */
    public static void addMarker(double latitude, double longitude, String text) {
        mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title(text));
    }

    /**
     * set the map to the current one
     * @param mMap
     */
    public static void setMap(GoogleMap mMap) {
        MapHandling.mMap = mMap;
    }



    /**
     * destroy the current status when the
     * enquete is saved
     */
    public static void destroy(){
        mFusedLocationClient = null;
    }

}
