package com.example.savenqueteur.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;

import com.example.savenqueteur.R;

import com.example.savenqueteur.dto.Enquete;

import java.util.List;

public class AdapterInvestigationList extends ArrayAdapter<Enquete> {

    private List<Enquete> enquetesList;
    private CheckedTextView txtName;


    public AdapterInvestigationList(Context context, List<Enquete> enquetesList) {
        super(context, 0, enquetesList);
        this.enquetesList = enquetesList;
    }

    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {

        return getCustomView(position, convertView, parent);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);

    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            convertView = vi.inflate(R.layout.spinner_items, null);
        }

        Enquete enquete = enquetesList.get(position);

        if (enquete != null) {
            txtName = (CheckedTextView) convertView.findViewById(R.id.nameInvestigation);
            txtName.setText(enquete.getTitle());

        }

        return convertView;
    }

}

