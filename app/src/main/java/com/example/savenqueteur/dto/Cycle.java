package com.example.savenqueteur.dto;


import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;


@IgnoreExtraProperties
public class Cycle implements Serializable {

    private String id ;
    private String numCycle ;

    public Cycle(){}

    public Cycle(String numCycle){
        this.numCycle = numCycle ;
    }

    public Cycle(String id, String numCycle){
        this.id = id ;
        this.numCycle = numCycle ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumCycle() {
        return numCycle;
    }

    public void setNumCycle(String nbCycle) {
        this.numCycle = nbCycle;
    }

    @Override
    public String toString() {
        return "Cycle{" +
                "id='" + id + '\'' +
                ", numCycle='" + numCycle + '\'' +
                '}';
    }
}
