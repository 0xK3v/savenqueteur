package com.example.savenqueteur.dto;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;


@IgnoreExtraProperties
public class CategoryCours implements Serializable {

    private String id ;
    private String name ;

    public CategoryCours(){}

    public CategoryCours(String id, String name){
        this.id = id ;
        this.name = name ;
    }

    public CategoryCours(String name){
        this.name = name ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryCours{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
