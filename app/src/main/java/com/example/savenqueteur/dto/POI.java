package com.example.savenqueteur.dto;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;


@IgnoreExtraProperties
public class POI implements Serializable {

    private String id ;
    private String title ;
    private String description ;
    private String picture ;
    private String video ;
    private String audio ;
    private Position position ;

    public POI(){}

    public POI(String id, String title, String description, String picture, String video, String audio, Position position){
        this.id = id ;
        this.title = title ;
        this.description = description ;
        this.picture = picture ;
        this.video = video ;
        this.audio = audio;
        this.position = position ;
    }

    public POI(String title, String description, String picture, String video, String audio, Position position){
        this.title = title ;
        this.description = description ;
        this.picture = picture ;
        this.video = video ;
        this.audio = audio ;
        this.position = position ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getVideo() {
        return video;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getAudio() {
        return audio;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "POI{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", picture='" + picture + '\'' +
                ", video='" + video + '\'' +
                ", position=" + position +
                '}';
    }
}
