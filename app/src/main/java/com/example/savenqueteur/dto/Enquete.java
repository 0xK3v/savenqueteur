package com.example.savenqueteur.dto;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@IgnoreExtraProperties
public class Enquete implements Serializable{

    private String id ;
    private String title ;
    private String description ;
    private CategoryCours category ;
    private List<POI> pois ;
    private String idCycle ;

    public Enquete(){
        pois = new ArrayList<>() ;
    }

    public Enquete(String id, String title, String description, CategoryCours category, List<POI> pois, String idCycle){
        this.id = id ;
        this.title = title ;
        this.description = description ;
        this.category = category ;
        this.pois = pois ;
        this.idCycle = idCycle ;
    }

    public Enquete(String title, String description, CategoryCours category, List<POI> pois, String idCycle){
        this.title = title ;
        this.description = description ;
        this.category = category ;
        this.pois = pois ;
        this.idCycle = idCycle ;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CategoryCours getCategory() {
        return category;
    }

    public void setCategory(CategoryCours category) {
        this.category = category;
    }

    public List<POI> getPois() {
        return pois;
    }

    public void setPois(List<POI> pois) {
        this.pois = pois;
    }

    public String getIdCycle() {
        return idCycle;
    }

    public void setIdCycle(String idCycle) {
        this.idCycle = idCycle;
    }
}
