package com.example.savenqueteur.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.savenqueteur.R;


public class ChoiceFragment extends Fragment {

    //elements
    private Button btnSelection;
    private Button btnCreate;

    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_choice, container, false);


        //instantiate all the elements
        btnSelection = (Button) view.findViewById(R.id.btnSelect);
        btnCreate = (Button) view.findViewById(R.id.btnCreate);

        btnSelection.setOnClickListener(new SelectInvestigation());
        btnCreate.setOnClickListener(new CreateInvestigation());

        // Inflate the layout for this fragment
        return view;

    }

    /**
     * listener to Select Investigation button
     */
    private class SelectInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            MainActivity.setIsInUpdate(true);
            fragment = new InvestigationFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

    /**
     * listener to Create Investigation button
     */
    private class CreateInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            MainActivity.setIsInUpdate(false);

            fragment = new CreateInvestigationFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

}
