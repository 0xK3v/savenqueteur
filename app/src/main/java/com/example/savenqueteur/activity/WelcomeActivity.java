package com.example.savenqueteur.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.savenqueteur.features.Permissions;
import com.example.savenqueteur.R;
import com.example.savenqueteur.firebase.ResetDB;
import com.google.firebase.database.FirebaseDatabase;


public class WelcomeActivity extends AppCompatActivity {


    final private int CYCLEFRAGMENT = 1;

    private Button btnWelcome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        //SEED TO RESET THE ENTIRE DATABASE - UNCOMMENT AND LAUNCH APP AND RECOMMENT
        //ResetDB.resetDB();

        //ask for permissions
        Permissions permissions = new Permissions();
        permissions.checkPermissions(this);

        //initialize elements
        btnWelcome = (Button) findViewById(R.id.btnWelcome);

        btnWelcome.setOnClickListener(new Start());


    }


    /**
     * listener to sign in will compare the form's input with the authentication from firebase
     */
    private class Start implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            intent.putExtra("frgToLoad", CYCLEFRAGMENT);
            startActivity(intent);
        }
    }




}
