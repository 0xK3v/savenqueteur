package com.example.savenqueteur.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.savenqueteur.R;
import com.example.savenqueteur.dto.Enquete;
import com.example.savenqueteur.dto.POI;
import com.example.savenqueteur.dto.Position;
import com.example.savenqueteur.features.Camera;
import com.example.savenqueteur.firebase.DataListener;
import com.example.savenqueteur.firebase.StorageAudio;
import com.example.savenqueteur.firebase.StoragePicture;
import com.example.savenqueteur.logic.MapHandling;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;




public class PoiFragment extends Fragment {

    //elements
    private TextView label_nLocation ;
    private TextView label_eLocation ;
    private EditText edtxt_poiName ;
    private EditText edtxt_poiDesc ;

    private ImageButton img_takePicture ;

    private ImageButton img_takeVideo ;

    private ImageButton img_takeAudio ;


    private ImageButton img_btn_savePOI ;

    private ImageView img_pictureView ;

    //enquete's object
    private Enquete enquete ;
    private Position position ;
    private Camera camera ;

    private POI poi ;

    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;
    private Bundle bundle;
    private boolean update;
    private boolean pictureChanged;
    //private int index;

    private StoragePicture storePic;
    private StorageAudio storeAudio;

    private String filenamePOI_Cam;

    private String filenamePOI_Vid;

    private String filenamePOI_Aud;

    private String pathAudioFileStore;

    private String pathFileStore;

    public PoiFragment(){
        poi = new POI();
        poi.setPicture("");
        update = false ;
        //index = -1 ;
        pictureChanged = false ;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_poi, container, false);

        label_nLocation = (TextView) view.findViewById(R.id.NLocation);
        label_eLocation = (TextView) view.findViewById(R.id.ELocation);


        edtxt_poiName = (EditText) view.findViewById(R.id.poi_name_input) ;

        edtxt_poiDesc = (EditText) view.findViewById(R.id.poi_description_input);


        img_takePicture = (ImageButton) view.findViewById(R.id.addPhoto);

        img_takeVideo = (ImageButton) view.findViewById(R.id.addVideo);

        img_takeAudio = (ImageButton) view.findViewById(R.id.addAudio);

        img_btn_savePOI = (ImageButton) view.findViewById(R.id.savePoi);

        img_pictureView = (ImageView) view.findViewById(R.id.imageView);

        storePic = new StoragePicture();

        storeAudio = new StorageAudio();

        camera = new Camera();

        MapHandling.getUserLatLng(new DataListener() {
            @Override
            public void onSuccess(Object object) {
                Location location = (Location) object ;
                String nLocation = location.getLatitude() + " N";
                String eLocation = location.getLongitude() + " E";
                label_nLocation.setText(nLocation);
                label_eLocation.setText(eLocation);
                position = new Position(location.getLongitude(), location.getLatitude());
            }

            @Override
            public void onFailed(Object object) {

            }
        });

        img_takePicture.setOnClickListener(new TakePicture());
        img_btn_savePOI.setOnClickListener(new savePOI());

        img_takeVideo.setOnClickListener(new TakeVideo());
        img_takeAudio.setOnClickListener(new TakeAudio());

        return view ;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        pictureChanged = true;

        if (camera.getChoice() == "camera") {

            Bitmap picture = BitmapFactory.decodeFile(camera.getAbsPathPicture());

            Bitmap rotatedPic = null;
            try {
                rotatedPic = camera.rotatePicture(picture);
            } catch (IOException e) {
                e.printStackTrace();
            }

            img_pictureView.setImageBitmap(rotatedPic);
            //then encode the picture and add to the string
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            filenamePOI_Cam = "POI_" + timeStamp;
            pathFileStore = camera.getAbsPathPicture();

        }

        else {

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            filenamePOI_Cam = "POI_" + timeStamp;

            camera.addToImageViewGallery(requestCode, resultCode, getActivity(), img_pictureView, data);

            pathFileStore = camera.getImgDecodableString();
        }

        if (camera.getChoice() == "cameraVideo") {

            //encode the video and add to the string
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            filenamePOI_Vid = "POI_vid_" + timeStamp;
            pathFileStore = camera.getAbsPathVideo();


        }

    }

    /**
     * listener on the button to add a picture to the poi
     * you can choose between an import from the gallery
     * and to take the picture directly from your phone
     */
    private class TakePicture implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            TextView myTitle = new TextView(getContext());
            myTitle.setText(R.string.cameraPOITitle);
            myTitle.setTextSize(25);
            //myTitle.setTextColor(getResources().getColor(R.color.red_main));
            myTitle.setPadding(80,30,10,10);
            camera = new Camera();
            new AlertDialog.Builder(getActivity())
                    .setCustomTitle(myTitle)
                    .setMessage(R.string.cameraPOIDesc)
                    .setPositiveButton(R.string.cameraGalleryPicPOI, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            camera.setChoice("gallery");
                            camera.launchImportImage(PoiFragment.this);
                        }

                    })
                    .setNegativeButton(R.string.cameraTakePicPOI, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            camera.setChoice("camera");
                            camera.takePictureIntent(PoiFragment.this);
                        }
                    })
                    .show();
        }
    }

    /**
     * listener on the button to add a video to the poi
     * you can choose between an import from the gallery
     * and to take the video directly from your phone
     */
    private class TakeVideo implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            TextView myTitle = new TextView(getContext());
            myTitle.setText(R.string.videoPOITitle);
            myTitle.setTextSize(25);
            //myTitle.setTextColor(getResources().getColor(R.color.red_main));
            myTitle.setPadding(80,30,10,10);
            camera = new Camera();
            new AlertDialog.Builder(getActivity())
                    .setCustomTitle(myTitle)
                    .setMessage(R.string.videoPOIDesc)
                    .setPositiveButton(R.string.videoGalleryPicPOI, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            camera.setChoice("galleryVideo");
                            camera.launchImportImage(PoiFragment.this);
                        }

                    })
                    .setNegativeButton(R.string.videoTakePicPOI, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            camera.setChoice("cameraVideo");
                            camera.takeVideoIntent(PoiFragment.this);
                        }
                    })
                    .show();
        }
    }

    /**
     * listener on the button to add audio to the poi
     * you can choose between an import from the gallery
     * and to take audio directly from your phone
     */
    private class TakeAudio implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            fragment = new AudioRecordFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /**
     * listener to save the poi
     */
    private class savePOI implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            //get the current investigation
            enquete = MainActivity.getEnquete();

            //validate that there is a name for the POI
            if (formValidation()){


                //set the poi values
                poi.setTitle(edtxt_poiName.getText().toString());
                poi.setDescription(edtxt_poiDesc.getText().toString());


                filenamePOI_Aud = AudioRecordFragment.filenamePOI_Aud;
                pathAudioFileStore = AudioRecordFragment.pathFileStore;

                if (poi.getPosition() == null) {
                    poi.setPosition(position);
                }


                if(filenamePOI_Cam==null){
                    Log.d("no img will be saved", enquete.toString());
                }
                else {
                    poi.setPicture(filenamePOI_Cam);
                    storePic.uploadPicture(pathFileStore, filenamePOI_Cam);
                }


                if(filenamePOI_Vid==null){
                    Log.d("no vid will be saved", enquete.toString());
                }
                else {
                    poi.setVideo(filenamePOI_Vid);
                    storePic.uploadVideo(pathFileStore, filenamePOI_Vid);
                }


                if(filenamePOI_Aud==null){
                    Log.d("no audio will be saved", enquete.toString());
                }
                else {
                    poi.setAudio(filenamePOI_Aud);
                    storeAudio.uploadAudio(pathFileStore, filenamePOI_Aud);
                }

                enquete.getPois().add(poi);

                //MainActivity.setEnquete(enquete);

                //add the marker to the map
                MapHandling.addMarker(poi.getPosition().getLatitude(), poi.getPosition().getLongitude(), poi.getTitle());

                Toast.makeText(getActivity(), R.string.poiSaved, Toast.LENGTH_SHORT).show();

                //Go back to the previous view
                fragmentManager = getActivity().getSupportFragmentManager();
                if (fragmentManager.getBackStackEntryCount() > 0) {
                    FragmentManager.BackStackEntry first = fragmentManager
                            .getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1);
                    fragmentManager.popBackStack(first.getId(),
                            FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }

            }


        }
    }

    /**
     * form validation to check that everything is filled in for the form
     * @return
     */
    private boolean formValidation() {
        if (edtxt_poiName.getText().toString().equals("")) {
            Toast.makeText(getContext(), R.string.pleaseTitle, Toast.LENGTH_SHORT).show();
            return false;
        } //else if (investigationDescription.getText().toString().equals("")) {
        //Toast.makeText(getContext(), R.string.pleaseDesc, Toast.LENGTH_SHORT).show();
        //return false;
        //}
        return true;
    }
}
