package com.example.savenqueteur.activity;


import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.savenqueteur.dto.Enquete;
import com.example.savenqueteur.R;



public class CreateInvestigationFragment extends Fragment {

    //elements
    private Button createInvestigation;
    private EditText investigationName;
    private EditText investigationDescription;

    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_create, container, false);

        //instantiate all the elements
        createInvestigation = (Button) view.findViewById(R.id.createInvestigation);
        investigationName = (EditText) view.findViewById(R.id.investigation_name_input);
        investigationDescription = (EditText) view.findViewById(R.id.investigation_description);

        createInvestigation.setOnClickListener(new CreateInvestigation());

        // Inflate the layout for this fragment
        return view;
    }


    /**
     * Create Investigation -> Then continue to MapFragment
     */
    private class CreateInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            if(formValidation()){
                Enquete enquete = new Enquete();

                enquete.setTitle(investigationName.getText().toString());
                enquete.setDescription(investigationDescription.getText().toString());

                enquete.setIdCycle(MainActivity.getCycle().getId());
                enquete.setCategory(MainActivity.getCategory());

                MainActivity.setEnquete(enquete);

                Bundle bundle = new Bundle();
                bundle.putSerializable("enquete", enquete);

                fragment = new MapFragment();
                fragment.setArguments(bundle);

                fragmentManager = getActivity().getSupportFragmentManager();
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

        }

    }

    /**
     * form validation to check that everything is filled in for the form
     * @return
     */
    private boolean formValidation() {
        if (investigationName.getText().toString().equals("")) {
            Toast.makeText(getContext(), R.string.pleaseTitle, Toast.LENGTH_SHORT).show();
            return false;
        } //else if (investigationDescription.getText().toString().equals("")) {
            //Toast.makeText(getContext(), R.string.pleaseDesc, Toast.LENGTH_SHORT).show();
            //return false;
        //}
        return true;
    }


}
