package com.example.savenqueteur.activity;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.widget.Toast;


import com.example.savenqueteur.dto.Cycle;
import com.example.savenqueteur.firebase.CycleDB;
import com.example.savenqueteur.firebase.DataListener;
import com.example.savenqueteur.R;


public class CycleFragment extends Fragment {

    //elements
    private Button cycleUn;
    private Button cycleDeux;
    private Button cycleTrois;

    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_cycle, container, false);

        //instantiate all the elements
        cycleUn = (Button) view.findViewById(R.id.cycleUn);
        cycleDeux = (Button) view.findViewById(R.id.cycleDeux);
        cycleTrois = (Button) view.findViewById(R.id.cycleTrois);

        //Set On ClickListener buttons Cycle1, cycle2, cycl3
        cycleUn.setOnClickListener(new CycleUn());
        cycleDeux.setOnClickListener(new CycleDeux());
        cycleTrois.setOnClickListener(new CycleTrois());

        // Inflate the layout for this fragment
        return view;

    }

    /**
     * listener to cycle 1 button
     */
    private class CycleUn implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            //Retrieve from DB cycle1
            CycleDB.getCycleByName("cycle1", new DataListener() {
                @Override
                public void onSuccess(Object object) {
                    MainActivity.setCycle((Cycle) object);
                }

                @Override
                public void onFailed(Object object) {

                }
            });

            fragment = new TypeFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

    /**
     * listener to cycle 2 button
     */
    private class CycleDeux implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            //Retrieve from DB cycle2
            CycleDB.getCycleByName("cycle2", new DataListener() {
                @Override
                public void onSuccess(Object object) {
                    MainActivity.setCycle((Cycle) object);
                }

                @Override
                public void onFailed(Object object) {

                }
            });

            fragment = new TypeFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /**
     * listener to cycle 3 button
     */
    private class CycleTrois implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            //Retrieve from DB cycle3
            CycleDB.getCycleByName("cycle3", new DataListener() {
                @Override
                public void onSuccess(Object object) {
                    MainActivity.setCycle((Cycle) object);
                }

                @Override
                public void onFailed(Object object) {

                }
            });

            fragment = new TypeFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }


}
