package com.example.savenqueteur.activity;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.savenqueteur.dto.Enquete;
import com.example.savenqueteur.firebase.DataListener;
import com.example.savenqueteur.firebase.EnqueteDB;
import com.example.savenqueteur.R;
import com.example.savenqueteur.adapter.AdapterInvestigationList;

import java.util.List;


public class InvestigationFragment extends Fragment {


    //elements
    private Spinner spinner;
    private List<Enquete> enquetes;
    private AdapterInvestigationList adapterInvestigation;
    private Button okButton;
    private Enquete enquete;

    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_investigation, container, false);

        //instantiate all the elements
        spinner = (Spinner) view.findViewById(R.id.investigation_spinner);
        okButton = (Button) view.findViewById(R.id.btnSelectInvestigation);

        okButton.setOnClickListener(new SelectInvestigation());

        EnqueteDB.getAllEnquetes(new DataListener() {

            @Override
            public void onSuccess(Object object) {
                enquetes = (List<Enquete>) object ;
                adapterInvestigation = new AdapterInvestigationList(getContext(), enquetes);
                spinner.setAdapter(adapterInvestigation);
                spinner.setOnItemSelectedListener(new ItemSelectInvestigation());

            }



            @Override
            public void onFailed(Object object) {

            }
        });

        // Inflate the layout for this fragment
        return view;

    }


    /**
     * listener to click on an item to get the details of an investigation
     */

    private class ItemSelectInvestigation implements AdapterView.OnItemSelectedListener {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            enquete = (Enquete) adapterView.getItemAtPosition(i);
            MainActivity.setEnquete(enquete);

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    }

    /**
     * listener to OK button
     */
    private class SelectInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            if(MainActivity.getEnquete() != null){
                //MainActivity.setEnquete(enquete);
                Bundle bundle = new Bundle();
                bundle.putSerializable("enquete", enquete);

                //create the fragment and add the bundle to the arguments
                fragment = new MapFragment();
                fragmentManager = getActivity().getSupportFragmentManager();
                fragment.setArguments(bundle);

                //switch to the new fragment
                transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.main_container, fragment).addToBackStack(null).commit();
            }else{
                Toast.makeText(getActivity(), R.string.selectOrCreateEnquete, Toast.LENGTH_SHORT).show();
            }
        }

    }

}
