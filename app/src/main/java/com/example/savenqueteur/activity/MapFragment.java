package com.example.savenqueteur.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.savenqueteur.R;
import com.example.savenqueteur.dto.Enquete;
import com.example.savenqueteur.firebase.DataListener;
import com.example.savenqueteur.firebase.EnqueteDB;
import com.example.savenqueteur.logic.MapHandling;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;


public class MapFragment extends Fragment implements OnMapReadyCallback{

    //Maps objects
    private GoogleMap mMap;
    private LocationManager locationManager;
    private ImageButton btnAddPoi;
    private ImageButton btnSaveEnquete;
    private TextView titleMap;
    private Bundle bundle;
    private Enquete enquete;


    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;


    public MapFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_map, container, false);


        //instantiate map view
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.Map);
        mapFragment.getMapAsync(this);

        //location manager
        locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);

        //instanciate all the elements
        btnAddPoi = (ImageButton) view.findViewById(R.id.btnAddPoi);
        btnSaveEnquete = (ImageButton) view.findViewById(R.id.btnSaveEnquete);
        titleMap = (TextView) view.findViewById(R.id.titleMap);

        MapHandling.getInstance(getActivity());

        //Retrieve enquete
        bundle = getArguments();
        enquete = (Enquete) bundle.getSerializable("enquete");

        titleMap.setText(enquete.getTitle());

        btnAddPoi.setOnClickListener(new AddPoiButton());
        btnSaveEnquete.setOnClickListener(new SaveEnqueteButton());

        return view;
    }


    /**
     * instantiage the map on map ready async method
     *
     * @param googleMap
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        MapHandling.setMap(mMap);
        try {
            mMap.setMyLocationEnabled(true);
            }
        catch (SecurityException e) {

        }

        if(MainActivity.getEnquete().getPois()!=null) {
            MapHandling.moveCameraToPoi();
        }
        else {
            MapHandling.moveCameraToUserPosition();
        }

    }

    /**
     * Listener for the POI button
     */
    private class AddPoiButton implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            fragment = new PoiFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    /**
     * Listener for the Save Button
     */
    private class SaveEnqueteButton implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            Toast.makeText(getActivity(), R.string.enqueteCreated, Toast.LENGTH_SHORT).show();
            if(MainActivity.isIsInUpdate()){
                //Need to update Enquete with the new POI
                EnqueteDB.deleteEnqueteById(MainActivity.getEnquete().getId(), new DataListener() {
                    @Override
                    public void onSuccess(Object object) {
                        EnqueteDB.createEnquete(MainActivity.getEnquete());
                    }

                    @Override
                    public void onFailed(Object object) {

                    }
                });
                //If Going back from MainActivity -> Removing the MapFragment from Stack so the user gets to the Creation/Selection page
                FragmentManager fm = getFragmentManager();
                fm.popBackStack();
                //When saving a request go back to MainActivity
                Intent fp = new Intent(getContext(), WelcomeActivity.class);
                startActivity(fp);
            }else{
                //Uncomment this to create it in the DB
                EnqueteDB.createEnquete(enquete);
                MainActivity.setEnquete(null);
                //If Going back from MainActivity -> Removing the MapFragment from Stack so the user gets to the Creation/Selection page
                FragmentManager fm = getFragmentManager();
                fm.popBackStack();
                //When saving a request go back to MainActivity
                Intent fp = new Intent(getContext(), WelcomeActivity.class);
                startActivity(fp);
            }

        }
    }

    /**
     * Delete Button for an investigation ? - Implemented but will use ?
     * If will be used - Need to check if in the MainActivity.isInUpdate
     */
    private class DeleteEnqueteButton implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            TextView myTitle = new TextView(getContext());
            myTitle.setText(R.string.delEnquete);
            myTitle.setTextSize(25);
            //myTitle.setTextColor(getResources().getColor(R.color.red_main));
            myTitle.setPadding(80,30,10,10);
            new AlertDialog.Builder(getActivity())
                    .setCustomTitle(myTitle)
                    .setMessage(R.string.delSureQues)
                    .setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }

                    })
                    .setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        EnqueteDB.deleteEnqueteById(MainActivity.getEnquete().getId(), new DataListener() {
                            @Override
                            public void onSuccess(Object object) {

                            }

                            @Override
                            public void onFailed(Object object) {

                            }
                        });
                        }
                    })
                    .show();
        }
    }

}
