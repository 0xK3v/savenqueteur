package com.example.savenqueteur.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;


import com.example.savenqueteur.dto.CategoryCours;
import com.example.savenqueteur.firebase.CategoryCoursDB;
import com.example.savenqueteur.firebase.DataListener;
import com.example.savenqueteur.R;


public class TypeFragment extends Fragment {

    //elements
    private Button btnScience;
    private Button btnHistory;
    private Button btnGeography;

    //fragments
    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_type, container, false);

        //instantiate all the elements
        btnScience = (Button) view.findViewById(R.id.btnScience);
        btnHistory = (Button) view.findViewById(R.id.btnHistory);
        btnGeography = (Button) view.findViewById(R.id.btnGeography);

        btnScience.setOnClickListener(new SciencesInvestigation());
        btnGeography.setOnClickListener(new GeographyInvestigation());
        btnHistory.setOnClickListener(new HistoryInvestigation());

        // Inflate the layout for this fragment
        return view;

    }


    /**
     * Listener for the "Geography" button
     */
    private class GeographyInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            CategoryCoursDB.getCategoryCoursByName("geographie", new DataListener() {
                @Override
                public void onSuccess(Object object) {
                    MainActivity.setCategory((CategoryCours) object);
                }

                @Override
                public void onFailed(Object object) {

                }
            });

            fragment = new ChoiceFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

    /**
     * Listener for the "History" button
     */
    private class HistoryInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            CategoryCoursDB.getCategoryCoursByName("histoire", new DataListener() {
                @Override
                public void onSuccess(Object object) {
                    MainActivity.setCategory((CategoryCours) object);
                }

                @Override
                public void onFailed(Object object) {

                }
            });

            fragment = new ChoiceFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

    /**
     * Listener for the "Sciences" button
     */
    private class SciencesInvestigation implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            CategoryCoursDB.getCategoryCoursByName("sciences", new DataListener() {
                @Override
                public void onSuccess(Object object) {
                    MainActivity.setCategory((CategoryCours) object);
                }

                @Override
                public void onFailed(Object object) {

                }
            });

            fragment = new ChoiceFragment();
            fragmentManager = getActivity().getSupportFragmentManager();
            transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.main_container, fragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }
}
