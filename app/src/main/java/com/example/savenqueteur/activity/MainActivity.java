package com.example.savenqueteur.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.example.savenqueteur.dto.CategoryCours;
import com.example.savenqueteur.dto.Cycle;
import com.example.savenqueteur.dto.Enquete;
import com.example.savenqueteur.R;

public class MainActivity extends AppCompatActivity {

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    final private int CYCLEFRAGMENT = 1;
    final private int TYPEFRAGMENT =2 ;
    final private int MAPFRAGMENT = 3;
    final private int POIFRAGMENT = 4;
    final private int INVESTIGATIONFRAGMENT = 5;
    final private int CREATEINVESTIGATIONFRAGMENT = 6;
    final private int CHOICEFRAGMENT = 7;


    //Investigation that will be used everywhere
    private static Enquete enquete;

    private static boolean isInUpdate ;

    public static boolean isIsInUpdate() {
        return isInUpdate;
    }

    public static void setIsInUpdate(boolean isInUpdate) {
        MainActivity.isInUpdate = isInUpdate;
    }

    //Variables to retrieve cycle, categories when clicking on them on the other fragments
    private static Cycle cycle ;
    private static CategoryCours category ;

    //Getter and Setter for the variables
    public static CategoryCours getCategory() {
        return category;
    }

    public static void setCategory(CategoryCours category) {
        MainActivity.category = category;
    }

    public static Cycle getCycle() {
        return cycle;
    }

    public static void setCycle(Cycle cycle) {
        MainActivity.cycle = cycle;
    }



    private static boolean isInMapFragment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        //everything linked to the fragments
        fragmentManager = getSupportFragmentManager();

        int intentFragment = getIntent().getExtras().getInt("frgToLoad");

        switch (intentFragment) {
            case CYCLEFRAGMENT:
                fragment = new CycleFragment();
                break;
            case TYPEFRAGMENT:
                fragment = new TypeFragment();
                break;
            case MAPFRAGMENT:
                enquete = null;
                fragment = new MapFragment();
                break;
            case POIFRAGMENT:
                fragment = new PoiFragment();
                break;
            case INVESTIGATIONFRAGMENT:
                fragment = new InvestigationFragment();
                break;
            case CREATEINVESTIGATIONFRAGMENT:
                fragment = new CreateInvestigationFragment();
                break;
            case CHOICEFRAGMENT:
                fragment = new ChoiceFragment();
                break;
        }
        transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.main_container, fragment).addToBackStack(null).commit();
    }

    public static Enquete getEnquete() {
        return MainActivity.enquete;
    }

    public static void setEnquete(Enquete enquete) {
        MainActivity.enquete = enquete;
    }

    public static void setIsInMapFragment(boolean isInMapFragment) {
        MainActivity.isInMapFragment = isInMapFragment;
    }
}

