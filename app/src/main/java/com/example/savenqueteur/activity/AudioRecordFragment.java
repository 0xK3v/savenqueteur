package com.example.savenqueteur.activity;


import android.app.FragmentTransaction;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.savenqueteur.R;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;



public class AudioRecordFragment extends Fragment {

    private ImageButton buttonStart, buttonStop, buttonPlayLastRecordAudio,
            buttonStopPlayingRecording, dismissRecord, recordOk ;

    private FragmentManager fragmentManager;
    private android.support.v4.app.FragmentTransaction transaction;

    static String pathFileStore = null;
    private MediaRecorder mediaRecorder ;

    private MediaPlayer mediaPlayer ;


    static String filenamePOI_Aud;



    public AudioRecordFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_audio_record, container, false);


        //init elements

        buttonStart = (ImageButton) view.findViewById(R.id.button);
        buttonStop = (ImageButton) view.findViewById(R.id.button2);
        buttonPlayLastRecordAudio = (ImageButton) view.findViewById(R.id.button3);
        buttonStopPlayingRecording = (ImageButton)view.findViewById(R.id.button4);

        recordOk = (ImageButton)view.findViewById(R.id.recordOk);
        dismissRecord = (ImageButton)view.findViewById(R.id.dismissRecord);

        recordOk.setEnabled(false);
        dismissRecord.setEnabled(true);


        buttonStop.setVisibility(view.INVISIBLE);
        buttonStopPlayingRecording.setVisibility(view.INVISIBLE);

        buttonStop.setEnabled(false);
        buttonPlayLastRecordAudio.setEnabled(false);
        buttonStopPlayingRecording.setEnabled(false);



        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                //encode the audio and add to the string
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                filenamePOI_Aud = "POI_aud_" + timeStamp;

                pathFileStore =  Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + filenamePOI_Aud;


                    MediaRecorderReady();

                    try {
                        mediaRecorder.prepare();
                        mediaRecorder.start();



                    } catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    buttonStop.setVisibility(view.VISIBLE);
                    buttonStart.setVisibility(view.INVISIBLE);


                    buttonStart.setEnabled(false);
                    buttonStop.setEnabled(true);


                Toast.makeText(getActivity(), "Recording started", Toast.LENGTH_SHORT).show();

            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaRecorder.stop();
                buttonStop.setEnabled(false);
                buttonPlayLastRecordAudio.setEnabled(true);
                buttonStart.setEnabled(true);
                buttonStopPlayingRecording.setEnabled(false);

                buttonStop.setVisibility(view.INVISIBLE);
                buttonStart.setVisibility(view.VISIBLE);

                recordOk.setEnabled(true);


                Toast.makeText(getActivity(), "Recording completed", Toast.LENGTH_SHORT).show();


            }
        });

        buttonPlayLastRecordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) throws IllegalArgumentException,
                    SecurityException, IllegalStateException {

                buttonStop.setEnabled(false);
                buttonStart.setEnabled(false);
                buttonStopPlayingRecording.setEnabled(true);

                mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(pathFileStore);
                    mediaPlayer.prepare();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                buttonStopPlayingRecording.setVisibility(view.VISIBLE);
                buttonPlayLastRecordAudio.setVisibility(view.INVISIBLE);

                mediaPlayer.start();
                Toast.makeText(getActivity(), "Recording playing", Toast.LENGTH_SHORT).show();

            }
        });

        buttonStopPlayingRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonStop.setEnabled(false);
                buttonStart.setEnabled(true);
                buttonStopPlayingRecording.setEnabled(false);
                buttonPlayLastRecordAudio.setEnabled(true);

                buttonStopPlayingRecording.setVisibility(view.INVISIBLE);
                buttonPlayLastRecordAudio.setVisibility(view.VISIBLE);

                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                    MediaRecorderReady();
                }

                Toast.makeText(getActivity(), "Recording stopped playing", Toast.LENGTH_SHORT).show();

            }
        });

        recordOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();

                Toast.makeText(getActivity(), "Recording saved", Toast.LENGTH_SHORT).show();

            }
        });


        dismissRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pathFileStore = null;
                fragmentManager = getActivity().getSupportFragmentManager();
                fragmentManager.popBackStack();

                Toast.makeText(getActivity(), "Recording dismissed", Toast.LENGTH_SHORT).show();

            }
        });


        return view;
    }

    public void MediaRecorderReady(){
        mediaRecorder=new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(pathFileStore);

    }

}
