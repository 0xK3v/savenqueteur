package com.example.savenqueteur.firebase;

import android.net.Uri;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnPausedListener;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;


//Picture Storage Class for firebase - storage
public class StoragePicture {

    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();

    private StorageMetadata metadata;

    /**
     * Upload a picture into the firebase storage
     * @param pathPicture
     * @param filename
     */

    public void uploadPicture(String pathPicture, String filename) {

        StorageReference picRef = storageRef.child(filename);
        InputStream stream = null;
        try {
            stream = new FileInputStream(new File(pathPicture));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        UploadTask uploadTask = picRef.putStream(stream);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        });
    }

    /**
     * Upload a video into the firebase storage
     * @param pathVideo
     * @param filename
     */
    public void uploadVideo(String pathVideo, String filename) {

        // Create the metadata file
        metadata = new StorageMetadata.Builder()
                .setContentType("video/mp4")
                .build();

        StorageReference videoRef = storageRef.child(filename);
        InputStream stream = null;
        try {
            stream = new FileInputStream(new File(pathVideo));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        UploadTask uploadTask = videoRef.putStream(stream, metadata);
        uploadTask.addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                System.out.println("Upload is " + progress + "% done");
            }
        }).addOnPausedListener(new OnPausedListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onPaused(UploadTask.TaskSnapshot taskSnapshot) {
                System.out.println("Upload is paused");
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // Handle successful uploads on complete
                Uri downloadUrl = taskSnapshot.getMetadata().getDownloadUrl();
            }
        });
    }

    /**
     * Download the picture from the firebase storage with the path
     * @param pathPicture
     * @param dataListener
     */

    public void downloadPicture(String pathPicture, final DataListener dataListener) {
        storageRef.child(pathPicture).getBytes(Long.MAX_VALUE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                dataListener.onSuccess(bytes);
            }
        });
    }

    /**
     * Deletes a picture from the firebase storage
     * @param pathPicture
     */

    public void deletePicture(String pathPicture) {
        storageRef.child(pathPicture).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }
}
