package com.example.savenqueteur.firebase;

import com.example.savenqueteur.dto.CategoryCours;
import com.example.savenqueteur.dto.Cycle;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by kevin on 23/04/18.
 */

public class ResetDB {

    private final static DatabaseReference DB_REFERENCE = FirebaseDatabase.getInstance().getReference();

    /**
     * this method bas used to reset all the data from the db
     */
    public static void resetDB() {
        //remove everything from DB
        DB_REFERENCE.removeValue();

        //Add all the Cycles -> 1,2,3
        CycleDB.createCycle(new Cycle("cycle1"));
        CycleDB.createCycle(new Cycle("cycle2"));
        CycleDB.createCycle(new Cycle("cycle3"));

        CategoryCoursDB.createCategory(new CategoryCours("geographie"));
        CategoryCoursDB.createCategory(new CategoryCours("histoire"));
        CategoryCoursDB.createCategory(new CategoryCours("sciences"));

    }
}
