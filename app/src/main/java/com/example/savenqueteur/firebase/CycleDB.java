package com.example.savenqueteur.firebase;

import android.support.annotation.NonNull;

import com.example.savenqueteur.dto.CategoryCours;
import com.example.savenqueteur.dto.Cycle;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by kevin on 27/03/18.
 */

public class CycleDB {

    private final static DatabaseReference CYCLE_REFERENCE = FirebaseDatabase.getInstance().getReference("cycles");

    private CycleDB(){}

    /**
     * Creates a Cycle into the db
     * @param cycle
     * @param dataListener
     */
    public static void createCycle(final Cycle cycle, final DataListener dataListener) {
        final DatabaseReference id = CYCLE_REFERENCE.push();
        id.setValue(cycle).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                getCycleById(id.getKey(), dataListener);
            }
        });
    }

    /**
     * Creates a Cycle without Listener
     * @param cycle
     */
    public static void createCycle(Cycle cycle) {
        final DatabaseReference id = CYCLE_REFERENCE.push();
        id.setValue(cycle);
    }

    /**
     * Get Cycle by id
     * @param id
     * @param dataListener
     */
    public static void getCycleById(final String id, final DataListener dataListener) {
        Query query = CYCLE_REFERENCE.child(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Cycle cycle = dataSnapshot.getValue(Cycle.class);
                cycle.setId(id);
                dataListener.onSuccess(cycle);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dataListener.onFailed(databaseError);
            }
        });
    }

    /**
     * Retrieve Cycle ID-Firebase by the choice of the user
     * @param name
     * @param dataListener
     */
    public static void getCycleByName(final String name, final DataListener dataListener){
        CYCLE_REFERENCE.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Cycle cycles = new Cycle();
                for (DataSnapshot trackSnapshot : dataSnapshot.getChildren()) {
                    Cycle cycle = trackSnapshot.getValue(Cycle.class);
                    if(cycle.getNumCycle().equals(name)){
                        cycles = cycle ;
                        cycles.setId(trackSnapshot.getKey());
                    }
                }
                dataListener.onSuccess(cycles);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



}
