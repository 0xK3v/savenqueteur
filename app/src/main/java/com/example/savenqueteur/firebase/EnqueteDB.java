package com.example.savenqueteur.firebase;

import android.support.annotation.NonNull;

import com.example.savenqueteur.dto.Enquete;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevin on 27/03/18.
 */

public class EnqueteDB {

    private final static DatabaseReference ENQUETE_REFERENCE = FirebaseDatabase.getInstance().getReference("enquetes");

    private EnqueteDB(){}

    /**
     * Creates an "Enquete" in the firebase database
     * @param enquete
     * @param dataListener
     */
    public static void createEnquete(Enquete enquete, final DataListener dataListener) {
        final DatabaseReference id = ENQUETE_REFERENCE.push();
        id.setValue(enquete).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                getEnqueteById(id.getKey(), dataListener);
            }
        });
    }

    /**
     * Create an "Enquete" into the DB
     * @param enquete
     */
    public static void createEnquete(Enquete enquete) {
        final DatabaseReference id = ENQUETE_REFERENCE.push();
        id.setValue(enquete);
    }

    /**
     * Get an "Enquete" by id
     * @param id
     * @param dataListener
     */
    public static void getEnqueteById(final String id, final DataListener dataListener) {
        Query query = ENQUETE_REFERENCE.child(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Enquete enquete = dataSnapshot.getValue(Enquete.class);
                enquete.setId(id);
                dataListener.onSuccess(enquete);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dataListener.onFailed(databaseError);
            }
        });
    }


    /**
     * Get all "Enquete" by Cycle
     * @param idCycle
     * @param dataListener
     */
    public static void getAllEnqueteByIdCycle(final String idCycle, final DataListener dataListener){
        ENQUETE_REFERENCE.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Enquete> enquetes = new ArrayList<>();
                for (DataSnapshot trackSnapshot : dataSnapshot.getChildren()) {
                    Enquete enquete = trackSnapshot.getValue(Enquete.class);
                    if(enquete.getIdCycle().equals(idCycle)){
                        enquetes.add(enquete);
                    }
                }
                dataListener.onSuccess(enquetes);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Get all "Enquetes" from the firebase
     * @param dataListener
     */
    public static void getAllEnquetes(final DataListener dataListener) {

        ENQUETE_REFERENCE.keepSynced(true);

        ENQUETE_REFERENCE.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Enquete> enquetes = new ArrayList<>();
                for (DataSnapshot trackSnapshot : dataSnapshot.getChildren()) {
                    Enquete enquete = trackSnapshot.getValue(Enquete.class);
                    enquete.setId(trackSnapshot.getKey());
                    enquetes.add(enquete);
                }
                dataListener.onSuccess(enquetes);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void deleteEnqueteById(String id, final DataListener dataListener){
        ENQUETE_REFERENCE.child(id).removeValue();
        dataListener.onSuccess("Success");
    }
}
