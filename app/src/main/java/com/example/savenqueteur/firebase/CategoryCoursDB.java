package com.example.savenqueteur.firebase;

import android.support.annotation.NonNull;

import com.example.savenqueteur.dto.CategoryCours;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevin on 27/03/18.
 */

public class CategoryCoursDB {

    private final static DatabaseReference CATEGORY_REFERENCE = FirebaseDatabase.getInstance().getReference("categories");

    private CategoryCoursDB(){}

    /**
     * Create a category into firebase
     * data listener used to get the result asynchronously
     * @param category
     * @param dataListener
     */
    public static void createCategory(CategoryCours category, final DataListener dataListener) {
        final DatabaseReference id = CATEGORY_REFERENCE.push();
        id.setValue(category).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                getCategoryById(id.getKey(), dataListener);
            }
        });
    }

    /**
     * Creates the category into firebase
     * @param category
     */
    public static void createCategory(CategoryCours category) {
        final DatabaseReference id = CATEGORY_REFERENCE.push();
        id.setValue(category);
    }

    /**
     * Get all categories from the DB
     * @param dataListener
     */
    public static void getAllCategories(final DataListener dataListener) {
        CATEGORY_REFERENCE.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<CategoryCours> categories = new ArrayList<>();
                for (DataSnapshot categorySnapshot : dataSnapshot.getChildren()) {
                    CategoryCours category = categorySnapshot.getValue(CategoryCours.class);
                    category.setId(categorySnapshot.getKey());
                    categories.add(category);
                }
                dataListener.onSuccess(categories);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dataListener.onFailed(databaseError);
            }
        });
    }

    /**
     * Get category by id from the DB
     * @param id
     * @param dataListener
     */
    public static void getCategoryById(final String id, final DataListener dataListener) {
        Query query = CATEGORY_REFERENCE.child(id);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CategoryCours category = dataSnapshot.getValue(CategoryCours.class);
                category.setId(id);
                dataListener.onSuccess(category);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                dataListener.onFailed(databaseError);
            }
        });
    }

    public static void getCategoryCoursByName(final String name, final DataListener dataListener){
        CATEGORY_REFERENCE.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                CategoryCours categories = new CategoryCours();
                for (DataSnapshot trackSnapshot : dataSnapshot.getChildren()) {
                    CategoryCours category = trackSnapshot.getValue(CategoryCours.class);
                    if(category.getName().equals(name)){
                        categories = category ;
                    }
                }
                dataListener.onSuccess(categories);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
