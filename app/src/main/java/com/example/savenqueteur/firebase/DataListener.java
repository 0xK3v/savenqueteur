package com.example.savenqueteur.firebase;

/**
 * Created by kevin on 27/03/18.
 */

public interface DataListener {

    void onSuccess(Object object);
    void onFailed(Object object);

}
